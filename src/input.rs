use std::fs;
use std::io;
use std::io::BufRead;
use std::path::Path;
use std::str;
use std::ops::Range;

use errors::*;

#[derive(Clone, Copy)]
pub enum FieldDelimiter {
    /// Parses spaces and tabs
    Whitespace,
    /// Parses the byte given as a record terminator.
    Any(u8),
}

impl PartialEq<u8> for FieldDelimiter{
    #[inline]
    fn eq(&self, &other: &u8) -> bool {
        match *self {
            FieldDelimiter::Whitespace => other == b' ' || other == b'\t',
            FieldDelimiter::Any(b) => other == b
        }
    }
}

impl<'a> PartialEq<Option<&'a u8>> for FieldDelimiter {
    #[inline]
    fn eq(&self, &other: &Option<&'a u8>) -> bool {
        if let Some(o) = other {
            match *self {
                FieldDelimiter::Whitespace=> *o == b' ' || *o == b'\t',
                FieldDelimiter::Any(b) => *o == b
            }
        } else {
            false
        }
    }
}

#[derive(Clone, Copy)]
pub enum RecordTerminator {
    /// Parses `\r`, `\n` or `\r\n` as a single record terminator.
    CRLF,
    /// Parses the byte given as a record terminator.
    Any(u8),
}

impl RecordTerminator {
    #[inline]
    fn is_crlf(&self) -> bool {
        match *self {
            RecordTerminator::CRLF => true,
            RecordTerminator::Any(_) => false,
        }
    }
}

impl PartialEq<u8> for RecordTerminator {
    #[inline]
    fn eq(&self, &other: &u8) -> bool {
        match *self {
            RecordTerminator::CRLF => other == b'\r' || other == b'\n',
            RecordTerminator::Any(b) => other == b
        }
    }
}

impl<'a> Into<u8> for RecordTerminator {
    fn into(self) -> u8 {
        match self {
            RecordTerminator::CRLF => b'\n',
            RecordTerminator::Any(x) => x
        }
    }
}


pub struct Reader<R> {
    rdr: io::BufReader<R>,
    delimiter: FieldDelimiter,
    record_term: RecordTerminator,
    eof: bool,
}

impl<R: io::Read> Reader<R> {
    pub fn from_reader(r: R) -> Reader<R> {
        Reader{
            rdr: io::BufReader::new(r),
            delimiter: FieldDelimiter::Whitespace,
            record_term: RecordTerminator::Any(b'\n'), // could use CRLF by default I guess
            eof: false,
        }
    }
}

impl Reader<io::Cursor<Vec<u8>>> {
    /// Creates a reader for an in memory string buffer.
    pub fn from_string<S>(s: S) -> Reader<io::Cursor<Vec<u8>>>
            where S: Into<String> {
        Reader::from_bytes(s.into().into_bytes())
    }

    /// Creates a reader for an in memory buffer of bytes.
    pub fn from_bytes<V>(bytes: V) -> Reader<io::Cursor<Vec<u8>>>
            where V: Into<Vec<u8>> {
        Reader::from_reader(io::Cursor::new(bytes.into()))
    }
}

impl Reader<fs::File> {
    /// Create a new Reader from a file.
    pub fn from_file<P: AsRef<Path>>(path: P) -> Result<Reader<fs::File>> {
        Ok(Reader::from_reader(try!(fs::File::open(path).chain_err(||
            "Could not open file for reading."))))
    }
}

impl<R: io::Read> Reader<R> {
      pub fn record_terminator(mut self, term: RecordTerminator) -> Reader<R> {
        self.record_term = term;
        self
    }

    pub fn delimiter(mut self, delim: u8) -> Reader<R> {
        self.delimiter = FieldDelimiter::Any(delim);
        self
    }
}

#[derive(Clone, Debug)]
pub struct Record {
    pub record: Vec<u8>,
    pub fields: Vec<Range<usize>>
}

impl Record {
    pub fn from_string(record: &str, delim: FieldDelimiter) -> Record {
        let v = record.bytes().collect::<Vec<_>>();
        Record::from_bytestring(v, delim)
    }

    pub fn from_bytestring(record: Vec<u8>, delim: FieldDelimiter) -> Record {
        let fields = Record::build_fields(&record, delim);
        Record {record: record, fields: fields}
    }

    fn build_fields(record: &[u8], delim: FieldDelimiter) -> Vec<Range<usize>> {
        let mut fields = vec![];
        let mut begin = 0;
        let mut in_field = false;
        for (i, c) in record.iter().enumerate() {
            match (in_field, delim == *c, delim) {
                (true, true, _) => {
                    fields.push(begin..i);
                    in_field = false;
                },
                (false, true, FieldDelimiter::Whitespace) => (), // consume whitespace
                (false, true, FieldDelimiter::Any(_)) => fields.push(i..i),
                (false, false, _) => {
                    begin = i;
                    in_field = true;
                },
                (true, false, _) => (), // still reading field
            }
        }
        fields.push(begin..record.len());
        fields
    }

    pub fn field(&self, idx: usize) -> &[u8] {
        let range = &self.fields[idx];
        &self.record[range.start..range.end]
    }
}

impl<R: io::Read> Reader<R> {
    fn next_record(&mut self) -> Result<Record> {
        let mut record = Vec::with_capacity(1024);
        let bytes_read = try!(self.rdr.read_until(self.record_term.clone().into(), &mut record).chain_err(||
            "Could not read from input."));
        if bytes_read == 0 {
            self.eof = true;
        }
        if self.record_term.is_crlf() {
            // ignore \r
            self.rdr.consume(1);
        }
        self.strip_record_end(&mut record);
        Ok(Record::from_bytestring(record, self.delimiter))
    }

    fn strip_record_end(&self, record: &mut Vec<u8>) {
        // Strip off the record terminator if it's there.
        // It won't be there on the last record.
        let reclen = record.len();
        if reclen == 0 {
            return;
        }
        match self.record_term {
            RecordTerminator::CRLF => {
                if record[reclen-1] == b'\n' {
                    record.truncate(reclen-1);
                }
            },
            RecordTerminator::Any(r) => {
                if record[reclen-1] == r {
                    record.truncate(reclen-1);
                }
            },
        };
    }
}

impl<R> Iterator for Reader<R> where R: io::Read {
    type Item = Result<Record>;

    fn next(&mut self) -> Option<Result<Record>> {
        let record = self.next_record();
        if self.eof {
            return None;
        }
        Some(record)
    }
}

#[cfg(test)]
mod test {
    use super::{FieldDelimiter, Reader, Record};

    #[test]
    fn test_record_construction() {
        let record = Record::from_string("abc,123", FieldDelimiter::Any(b','));
        let expected = vec![b"abc", b"123"];
        let mut seen = 0;
        for i in 0..record.fields.len() {
            println!("record: {:?}, expected; {:?}", record.field(i), expected[i]);
            assert_eq!(record.field(i), expected[i]);
            seen += 1;
        }
        assert_eq!(seen, expected.len());
    }

    #[test]
    fn test_fields() {
        let rdr = Reader::from_string("abc,123").delimiter(b',');
        let expected = vec![["abc", "123"]];
        let mut seen = 0;
        for (i, rec) in rdr.enumerate() {
            let rec = rec.unwrap();
            for j in 0..rec.fields.len() {
                let e : Vec<u8> = expected[i][j].bytes().collect();
                assert_eq!(e, rec.field(j)); 
                seen += 1;
            }
        }
        assert_eq!(seen, expected.iter().map(|v|v.len()).sum::<usize>());
    }

    #[test]
    fn test_fields_default_whitespace_delimiter_is_greedy() {
        let record = Record::from_string("abc        123", FieldDelimiter::Whitespace);
        let expected = vec![b"abc", b"123"];
        let mut seen = 0;
        for i in 0..record.fields.len() {
            assert_eq!(record.field(i), expected[i]);
            seen += 1;
        }
        assert_eq!(seen, expected.len());
    }

    #[test]
    #[should_panic] // TODO: fix
    fn test_fields_trailiing_whitespace_does_not_imply_new_field() {
        let record = Record::from_string("abc        123      ", FieldDelimiter::Whitespace);
        let expected = vec![b"abc", b"123"];
        let mut seen = 0;
        for i in 0..record.fields.len() {
            assert_eq!(record.field(i), expected[i]);
            seen += 1;
        }
        assert_eq!(seen, expected.len());
    }

    #[test]
    fn test_records() {
        let rdr = Reader::from_string("abc\n123");
        let expected = vec![[b"abc"], [b"123"]];
        let mut seen = 0;
        for (i, rec) in rdr.enumerate() {
            let rec = rec.unwrap();
            for j in 0..rec.fields.len() {
                assert_eq!(rec.field(j), expected[i][j]);
                seen += 1;
            }
        }
        assert_eq!(seen, expected.iter().map(|v|v.len()).sum::<usize>());
    }

    #[test]
    fn test_empty_are_records() {
        let rdr = Reader::from_string("abc\n\n\n123");
        let expected = vec![vec!["abc"], vec![""], vec![""], vec!["123"]];
        let mut seen = 0;
        for (i, rec) in rdr.enumerate() {
            let rec = rec.unwrap();
            println!("rec-record: {:?}", rec.record);
            for j in 0..rec.fields.len() {
                let e : Vec<u8> = expected[i][j].bytes().collect();
                assert_eq!(e, rec.field(j));
                seen += 1;
            }
        }
        assert_eq!(seen, expected.iter().map(|v|v.len()).sum::<usize>());
    }

    #[test]
    fn test_fields_and_records() {
        let rdr = Reader::from_string("abc 123\n123 abc");
        let expected = vec![["abc", "123"], ["123", "abc"]];
        let mut seen = 0;
        for (i, rec) in rdr.enumerate() {
            let rec = rec.unwrap();
            for j in 0..rec.fields.len() {
                let e : Vec<u8> = expected[i][j].bytes().collect();
                assert_eq!(e, rec.field(j));
                seen += 1;
            }
        }
        assert_eq!(seen, expected.iter().map(|v|v.len()).sum::<usize>());
    }

    #[test]
    fn test_fields_and_records_utf8() {
        let rdr = Reader::from_string("Хардбас Кашу\n北京 技术音乐");
        let expected = vec![["Хардбас", "Кашу"], ["北京", "技术音乐"]];
        let mut seen = 0;
        for (i, rec) in rdr.enumerate() {
            let rec = rec.unwrap();
            for j in 0..rec.fields.len() {
                let e : Vec<u8> = expected[i][j].bytes().collect();
                assert_eq!(e, rec.field(j));
                seen += 1;
            }
         }
        assert_eq!(seen, expected.iter().map(|v|v.len()).sum::<usize>());
     }
}
