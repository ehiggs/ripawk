use ast;
use vm;

use errors::*;

pub struct Compiler {
    // symboltable: HashMap<String, Val>
    pub ops: Vec<vm::OpCode>,
}

impl Compiler {
    pub fn new() -> Compiler {
        Compiler{ ops: Vec::new() }
    }

    pub fn compile_block(&mut self, block: &ast::Block) -> Result<i64> {
        let mut sum_num_instr = 0;
        for stmt in &block.statements {
            let num_instr = match *stmt {
                ast::Statement::Expr(ref expr) => try!(self.compile_expr(expr)),
                ast::Statement::If(ref if_stmt) => try!(self.compile_if(if_stmt)),
                ast::Statement::WhileLoop(ref while_loop) => try!(self.compile_while_loop(while_loop)),
                ast::Statement::DoLoop(ref do_loop) => try!(self.compile_do_loop(do_loop)),
                ast::Statement::ForLoop(ref for_loop) => try!(self.compile_for_loop(for_loop)),
                ast::Statement::Print(ref expr) => try!(self.compile_print(expr)),
                _ => panic!("Unhandled statement type: {:?}", stmt),
            };
            sum_num_instr += num_instr;
        }
        Ok(sum_num_instr)
    }

    fn compile_expr(&mut self, expr: &ast::Expr) -> Result<i64> {
        let num_instr = match *expr {
            ast::Expr::Number(n) => self.compile_loadconstint(n)?,
            ast::Expr::Id(ref i) => self.compile_loadint(&i.0)?,
            ast::Expr::BinExpr(ref be) => self.compile_binexpr(be)?,
            ast::Expr::UnaryExpr(ref ue) => self.compile_unaryexpr(ue)?,
            ast::Expr::UnaryIncr(ref ue) => self.compile_unaryincr(ue)?,
            ast::Expr::Assignment(ref a) => self.compile_assignment(a)?,
            ast::Expr::FunCall(ref f) => self.compile_funcall(f)?,
        };
        Ok(num_instr)
    }

    #[inline]
    fn compile_loadconstint(&mut self, n: i64) -> Result<i64> {
        self.ops.push(vm::OpCode::LoadConstInt(n));
        Ok(1)
    }

    #[inline]
    fn compile_loadint(&mut self, sym: &str) -> Result<i64> {
        self.ops.push(vm::OpCode::LoadInt(sym.to_string()));
        Ok(1)
    }

    fn compile_binexpr(&mut self, binexpr: &ast::BinExpr) -> Result<i64> {
        let lhs_num_instr = try!(self.compile_expr(&*binexpr.lhs));
        let rhs_num_instr = try!(self.compile_expr(&*binexpr.rhs));
        let binop = match binexpr.op {
            ast::BinOp::Add => vm::OpCode::Add,
            ast::BinOp::Sub => vm::OpCode::Sub,
            ast::BinOp::Mul => vm::OpCode::Mul,
            ast::BinOp::Div => vm::OpCode::Div,
            ast::BinOp::Mod => vm::OpCode::Mod,
            ast::BinOp::Pow => vm::OpCode::Pow,
            ast::BinOp::And => vm::OpCode::Compare(ast::BinOp::And),
            ast::BinOp::Or  => vm::OpCode::Compare(ast::BinOp::Or),
            ast::BinOp::Eq => vm::OpCode::Compare(ast::BinOp::Eq),
            ast::BinOp::Neq => vm::OpCode::Compare(ast::BinOp::Neq),
            ast::BinOp::Less => vm::OpCode::Compare(ast::BinOp::Less),
            ast::BinOp::LessEqual => vm::OpCode::Compare(ast::BinOp::LessEqual),
            ast::BinOp::Greater => vm::OpCode::Compare(ast::BinOp::Greater),
            ast::BinOp::GreaterEqual => vm::OpCode::Compare(ast::BinOp::GreaterEqual),
        };
        self.ops.push(binop);
        Ok(lhs_num_instr + rhs_num_instr + 1)
    }

    fn compile_unaryexpr(&mut self, unaryexpr: &ast::UnaryExpr) -> Result<i64> {
        let expr_num_instr = try!(self.compile_expr(&*unaryexpr.expr));
        let unaryop = match unaryexpr.op {
            ast::UnaryExprOp::Negate  => vm::OpCode::Neg,
            ast::UnaryExprOp::Not     => vm::OpCode::Not,
        };
        self.ops.push(unaryop);
        Ok(expr_num_instr + 1)
    }

    fn compile_unaryincr(&mut self, unaryincr: &ast::UnaryIncr) -> Result<i64> {
        let id = &unaryincr.lvalue.0;
        self.ops.push(vm::OpCode::LoadInt(id.clone()));
        let unaryop = match unaryincr.op {
            ast::UnaryIncrOp::PreDecr => vm::OpCode::Decr,
            ast::UnaryIncrOp::PreIncr => vm::OpCode::Incr,
            _ => panic!("Post increment/decrement isn't supported yet!"),
        };
        self.ops.push(unaryop);
        self.ops.push(vm::OpCode::StoreInt(id.clone()));
        Ok(3)
    }

    fn compile_assignment(&mut self, assign: &ast::Assignment) -> Result<i64> {
        let num_rhs_instr = try!(self.compile_expr(&*assign.rvalue));
        let num_lhs_instr = match assign.op {
            ast::AssOp::Assign => {
                self.ops.push(vm::OpCode::StoreInt(assign.lvalue.0.clone()));
                1
            },
            ast::AssOp::Add => {
                self.ops.push(vm::OpCode::LoadInt(assign.lvalue.0.clone()));
                self.ops.push(vm::OpCode::Add);
                self.ops.push(vm::OpCode::StoreInt(assign.lvalue.0.clone()));
                3
            },
            ast::AssOp::Sub => {
                self.ops.push(vm::OpCode::LoadInt(assign.lvalue.0.clone()));
                self.ops.push(vm::OpCode::Sub);
                self.ops.push(vm::OpCode::StoreInt(assign.lvalue.0.clone()));
                3
            },
            ast::AssOp::Mul =>{
                self.ops.push(vm::OpCode::LoadInt(assign.lvalue.0.clone()));
                self.ops.push(vm::OpCode::Mul);
                self.ops.push(vm::OpCode::StoreInt(assign.lvalue.0.clone()));
                3
            },
            ast::AssOp::Div =>{
                self.ops.push(vm::OpCode::LoadInt(assign.lvalue.0.clone()));
                self.ops.push(vm::OpCode::Div);
                self.ops.push(vm::OpCode::StoreInt(assign.lvalue.0.clone()));
                3
            },
            ast::AssOp::Mod =>{
                self.ops.push(vm::OpCode::LoadInt(assign.lvalue.0.clone()));
                self.ops.push(vm::OpCode::Mod);
                self.ops.push(vm::OpCode::StoreInt(assign.lvalue.0.clone()));
                3
            },
        };
        Ok(num_rhs_instr + num_lhs_instr)
    }

    /// This is a nasty algorithm. 
    /// 1. we push the conditional to the top of the stack; 
    /// 1. we add the then part of the if statement. 
    /// 3. We then add a jump to skip an else (if there's an else.
    /// 4. We then edit the jump in the original conditional to reflect the number of instructions
    ///    we need to skip.
    /// NB: Forward jumps have a +1 tacked onto them so we advance to the next statement.
    fn compile_if(&mut self, if_stmt: &ast::IfStatement) -> Result<i64> {
        let cond_num_instr = try!(self.compile_cond(&if_stmt.cond));
        let placeholder_idx = self.ops.len() - 1;
        let then_num_instr = try!(self.compile_block(&if_stmt.then));
        let else_num_instr = match if_stmt.else_ {
            Some(ref else_) => {
                self.ops.push(vm::OpCode::Jump(1));
                let skip_else = self.ops.len() - 1;
                let else_num_instr = try!(self.compile_block(else_));
                let x = try!(self.ops.get_mut(skip_else).ok_or("The jump over the else disappeared!?"));
                *x = vm::OpCode::Jump(else_num_instr + 1);
                else_num_instr
            }
            None => 0
        };
        // The 'then' clause will also add a Jump if there is an else statement. We need to skip
        // that too.
        let skip_then_jump = if else_num_instr > 0 { 1 } else { 0 };
        if let Some(jump) = self.ops.get_mut(placeholder_idx) {
            match *jump {
                vm::OpCode::JumpIfTrue(_) => *jump = vm::OpCode::JumpIfTrue(then_num_instr + skip_then_jump + 1),
                vm::OpCode::JumpIfFalse(_) => *jump = vm::OpCode::JumpIfFalse(then_num_instr + skip_then_jump + 1),
                _ => panic!("We edited the wrong instruction!")
            }
        }
        Ok(cond_num_instr + then_num_instr + else_num_instr)
    }

    fn compile_cond(&mut self, cond: &ast::Expr) -> Result<i64> {
        let num_instr = self.compile_comparison(cond)?;
        Ok(num_instr)
    }

    fn compile_comparison(&mut self, comp: &ast::Expr) -> Result<i64> {
        let num_instr = self.compile_expr(&*comp)?;
        self.ops.push(vm::OpCode::JumpIfFalse(1));
        Ok(num_instr + 1)
    }

    fn compile_print(&mut self, expr: &ast::Expr) -> Result<i64> {
        let num_instr = try!(self.compile_expr(expr));
        self.ops.push(vm::OpCode::Print);
        Ok(num_instr + 1)
    }

    fn compile_while_loop(&mut self, while_loop: &ast::WhileLoop) -> Result<i64> {
        let cond_num_instr = try!(self.compile_cond(&while_loop.cond));
        let cond_jump_idx = self.ops.len() - 1;
        let body_num_instr = try!(self.compile_block(&while_loop.body));
        self.ops.push(vm::OpCode::Jump(-(body_num_instr + cond_num_instr)));
        let cond_jump = try!(self.ops.get_mut(cond_jump_idx).ok_or("The jump for the loop disappeared!?"));
        *cond_jump = vm::OpCode::JumpIfFalse(body_num_instr + 2);
        Ok(body_num_instr + cond_num_instr + 1)

    }

    fn compile_do_loop(&mut self, do_loop: &ast::DoLoop) -> Result<i64> {
        let body_num_instr = try!(self.compile_block(&do_loop.body));
        let cond_num_instr = try!(self.compile_cond(&do_loop.cond));
        let cond_jump  = try!(self.ops.last_mut().ok_or("The jump for the loop disappeared!?"));
        *cond_jump = vm::OpCode::JumpIfTrue(-(body_num_instr + cond_num_instr -1));
        Ok(body_num_instr + cond_num_instr)
    }

    fn compile_for_loop(&mut self, for_loop: &ast::ForLoop) -> Result<i64> {
        let init_num_instr = match for_loop.init {
            Some(ref expr) => try!(self.compile_expr(expr)),
            None => 0,
        };

        let cond_num_instr = match for_loop.cond {
            Some(ref cond) => try!(self.compile_cond(cond)),
            None => 0,
        }; 
        // should be -1 but if we are for(;;) then we could be empty. So -1 when setting the jump
        let cond_jump_idx = self.ops.len();

        let body_num_instr = try!(self.compile_block(&for_loop.body));

        let inc_num_instr = match for_loop.inc {
            Some(ref expr) => try!(self.compile_expr(expr)),
            None => 0,
        };
        self.ops.push(vm::OpCode::Jump(-(body_num_instr + cond_num_instr + inc_num_instr)));

        if cond_num_instr > 0 {
            // aforementioned '-1'
            let cond_jump = try!(self.ops.get_mut(cond_jump_idx-1).ok_or("The jump for the loop disappeared!?"));
            *cond_jump = vm::OpCode::JumpIfFalse(body_num_instr + inc_num_instr + 2);
        }
        Ok(init_num_instr + body_num_instr + cond_num_instr + inc_num_instr + 1)
    }

    fn compile_funcall(&mut self, funcall: &ast::FunCall) -> Result<i64> {
        let mut instructions = 0;
        self.ops.push(vm::OpCode::LoadFunction(funcall.name.0.to_string()));
        for arg in &funcall.args {
            instructions += self.compile_expr(arg)?;
        }
        self.ops.push(vm::OpCode::CallFunction(funcall.args.len()));
        Ok(instructions + 2)
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use ast;
    use grammar;
    use vm::OpCode;

    #[test]
    fn test_compile_expr_number() {
        let mut c = Compiler::new();
        let expected = vec![OpCode::LoadConstInt(5)];
        c.compile_expr(&ast::Expr::Number(5)).unwrap();
        assert_eq!(c.ops, expected);
    }

    #[test]
    fn test_compile_expr_id() {
        let mut c = Compiler::new();
        let expected = vec![OpCode::LoadInt("an_identifier".to_string())];
        c.compile_expr(&ast::Expr::Id(ast::Id("an_identifier".to_string()))).unwrap();
        assert_eq!(c.ops, expected);
    }

    #[test]
    fn test_compile_expr_binexpr_add() {
        let mut c = Compiler::new();
        let expected = vec![OpCode::LoadConstInt(1), OpCode::LoadInt("i".to_string()), OpCode::Add];
        c.compile_expr(&grammar::expression("1+i").unwrap()).unwrap();
        assert_eq!(c.ops, expected);
    }

    #[test]
    fn test_compile_expr_binexpr_sub() {
        let mut c = Compiler::new();
        let expected = vec![OpCode::LoadConstInt(72), OpCode::LoadInt("i".to_string()), OpCode::Sub];
        c.compile_expr(&grammar::expression("72-i").unwrap()).unwrap();
        assert_eq!(c.ops, expected);
    }

    #[test]
    fn test_compile_print() {
        let mut c = Compiler::new();
        let expected = vec![OpCode::LoadConstInt(5), OpCode::Print];
        c.compile_print(&ast::Expr::Number(5)).unwrap();
        assert_eq!(c.ops, expected);
    }

    #[test]
    fn test_compile_if_statement() {
        let mut c = Compiler::new();
        let expected = vec![
            OpCode::LoadInt("i".to_string()),
            OpCode::LoadConstInt(2),
            OpCode::Compare(ast::BinOp::Less),
            OpCode::JumpIfFalse(3),
            OpCode::LoadInt("i".to_string()),
            OpCode::Print,
        ];
        let if_string = "if (i < 2) { print i }";
        if let ast::Statement::If(if_stmt) = grammar::if_statement(if_string).unwrap() {
            c.compile_if(&if_stmt).unwrap();
        } else {
            assert!(false, "We should have parsed an if statement.");
        }
        assert_eq!(c.ops, expected);
    }

    #[test]
    fn test_compile_if_statement_with_else() {
        let mut c = Compiler::new();
        let expected = vec![
            OpCode::LoadInt("i".to_string()),
            OpCode::LoadConstInt(2),
            OpCode::Compare(ast::BinOp::Less),
            OpCode::JumpIfFalse(4),
            OpCode::LoadInt("i".to_string()),
            OpCode::Print,
            OpCode::Jump(3),
            OpCode::LoadConstInt(2),
            OpCode::Print
        ];
        let if_string = "if (i < 2) { print i } else { print 2}";
        if let ast::Statement::If(if_stmt) = grammar::if_statement(if_string).unwrap() {
            c.compile_if(&if_stmt).unwrap();
        } else {
            assert!(false, "We should have parsed an if statement.");
        }
        assert_eq!(c.ops, expected);
    }

    #[test]
    fn test_compile_block_with_if_else() {
        let mut c = Compiler::new();
        let expected = vec![
            OpCode::LoadInt("i".to_string()),
            OpCode::LoadConstInt(2),
            OpCode::Compare(ast::BinOp::Less),
            OpCode::JumpIfFalse(4),
            OpCode::LoadInt("i".to_string()),
            OpCode::Print,
            OpCode::Jump(3),
            OpCode::LoadConstInt(7),
            OpCode::Print
        ];
        let block_string = "if (i < 2) { print i } else { print 7 }";
        let block = grammar::block(block_string).unwrap();
        c.compile_block(&block).unwrap();
        assert_eq!(c.ops, expected);
    }

    #[test]
    fn test_compile_do_loop() {
        let mut c = Compiler::new();
        let expected = vec![
            OpCode::LoadInt("i".to_string()),
            OpCode::Incr,
            OpCode::StoreInt("i".to_string()),
            OpCode::LoadInt("i".to_string()),
            OpCode::LoadConstInt(5),
            OpCode::Compare(ast::BinOp::Less),
            OpCode::JumpIfTrue(-6),
        ];
        let do_string = "do ++i; while(i < 5)";
        let do_loop = grammar::block(do_string).unwrap();
        c.compile_block(&do_loop).unwrap();
        assert_eq!(c.ops, expected);
    }

    #[test]
    fn test_compile_while_loop() {
        let mut c = Compiler::new();
        let expected = vec![
            OpCode::LoadInt("i".to_string()),
            OpCode::LoadConstInt(5),
            OpCode::Compare(ast::BinOp::Less),
            OpCode::JumpIfFalse(5),
            OpCode::LoadInt("i".to_string()),
            OpCode::Incr,
            OpCode::StoreInt("i".to_string()),
            OpCode::Jump(-7),
        ];
        let while_string = "while(i < 5) ++i;";
        let while_loop = grammar::block(while_string).unwrap();
        c.compile_block(&while_loop).unwrap();
        assert_eq!(c.ops, expected);
    }

    #[test]
    fn test_compile_for_loop() {
        let mut c = Compiler::new();
        let expected = vec![
            // Init
            OpCode::LoadConstInt(0),
            OpCode::StoreInt("i".to_string()),

            // Cond
            OpCode::LoadInt("i".to_string()),
            OpCode::LoadConstInt(5),
            OpCode::Compare(ast::BinOp::Less),
            OpCode::JumpIfFalse(7),

            // Body
            OpCode::LoadInt("i".to_string()),
            OpCode::Print,

            // Incr
            OpCode::LoadInt("i".to_string()),
            OpCode::Incr,
            OpCode::StoreInt("i".to_string()),
            OpCode::Jump(-9),
        ];
        let for_string = "for(i = 0;i < 5; ++i) print i;";
        let for_loop = grammar::block(for_string).unwrap();
        c.compile_block(&for_loop).unwrap();
        assert_eq!(c.ops, expected);
    }

    #[test]
    fn test_compile_for_loop_no_init() {
        let mut c = Compiler::new();
        let expected = vec![
            // Cond
            OpCode::LoadInt("i".to_string()),
            OpCode::LoadConstInt(5),
            OpCode::Compare(ast::BinOp::Less),
            OpCode::JumpIfFalse(7),

            // Body
            OpCode::LoadInt("i".to_string()),
            OpCode::Print,

            // Incr
            OpCode::LoadInt("i".to_string()),
            OpCode::Incr,
            OpCode::StoreInt("i".to_string()),
            OpCode::Jump(-9),
        ];
        let for_string = "for(;i < 5; ++i) print i;";
        let for_loop = grammar::block(for_string).unwrap();
        c.compile_block(&for_loop).unwrap();
        assert_eq!(c.ops, expected);
    }

    #[test]
    fn test_compile_for_loop_no_init_no_inc() {
        let mut c = Compiler::new();
        let expected = vec![
            // Cond
            OpCode::LoadInt("i".to_string()),
            OpCode::LoadConstInt(5),
            OpCode::Compare(ast::BinOp::Less),
            OpCode::JumpIfFalse(4),

            // Body
            OpCode::LoadInt("i".to_string()),
            OpCode::Print,

            OpCode::Jump(-6),
        ];
        let for_string = "for(;i < 5;) print i;";
        let for_loop = grammar::block(for_string).unwrap();
        c.compile_block(&for_loop).unwrap();
        assert_eq!(c.ops, expected);
    }

    #[test]
    fn test_compile_for_loop_no_init_no_cond_no_inc() {
        let mut c = Compiler::new();
        let expected = vec![
            // Cond
            // Body
            OpCode::LoadInt("i".to_string()),
            OpCode::Print,

            OpCode::Jump(-2),
        ];
        let for_string = "for(;;) print i;";
        let for_loop = grammar::block(for_string).unwrap();
        c.compile_block(&for_loop).unwrap();
        assert_eq!(c.ops, expected);
    }

    #[test]
    #[should_panic]
    fn test_compile_incr() {
        let mut c = Compiler::new();
        let expected = vec![
            OpCode::LoadInt("i".to_string()),
            OpCode::Incr,
            OpCode::StoreInt("i".to_string()),
        ];
        let incr_string = "i++";
        let incr = grammar::expression(incr_string).unwrap();
        c.compile_expr(&incr).unwrap();
        assert_eq!(c.ops, expected);
    }
}
