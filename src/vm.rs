use std::collections::HashMap;
use std::str;
use regex::Regex;
use errors::*;
use input::Record;

use ast;
use compile;

// I would like this to be Copy as well but String doesn't implement Copy.
// Maybe make it Cow? Probably.. symbols don't change much/at all.
#[derive(Clone, PartialEq, Eq, Debug)]
pub enum OpCode {
    LoadInt(String),
    StoreInt(String),
    LoadConstInt(i64),
    LoadFunction(String),
    Incr,
    Decr,
    Neg,
    Add,
    Sub,
    Mul,
    Div,
    Mod,
    Pow,
    Not,
    Compare(ast::BinOp),
    Jump(i64),
    JumpIfTrue(i64),
    JumpIfFalse(i64),
    CallFunction(usize),
    Print,
    Next,
    Return,
    ReturnVal,
    PlaceHolder, // To be replaced
}

#[derive(Clone, Debug)]
enum Val {
    Float(f32),
    Int(i64),
    String(String),
}

#[derive(Debug)]
struct SymbolTable(HashMap<String, Val>);

#[derive(Debug)]
struct Stack { 
    syms: SymbolTable,
    state: Vec<i64>,
}

impl Stack { 
    fn new() -> Stack {
        Stack { 
            syms: SymbolTable(HashMap::new()),
            state: Vec::new(),
        }
    }

    fn run_instr(&mut self, instr: OpCode) -> Result<i64> {
        match instr {
            OpCode::LoadInt(sym) => {
                match self.syms.get(&sym) {
                    Val::Int(x) => self.state.push(x),
                    _ => panic!("Unsupported type attempted to load as int"),
                }
            },
            OpCode::StoreInt(sym) => {
                if let Some(x) = self.state.pop() {
                    self.syms.set(&sym, Val::Int(x));
                } else {
                    panic!("Attempted to store a variable but the stack was empty!");
                }
            },
            OpCode::LoadConstInt(x) => self.state.push(x),
            OpCode::Incr => {
                let mut last = try!(self.state.last_mut().ok_or("Could not access the previous to Incr."));
                *last +=1; 
            }, 
            OpCode::Decr => {
                let mut last = try!(self.state.last_mut().ok_or("Could not access the previous to Decr."));
                *last -= 1;
            }
            OpCode::Neg => if let Some(last) = self.state.last_mut() { *last *= -1; },
            OpCode::Add => {
                let rhs = try!(self.state.pop().ok_or("Could not find lhs for Add"));
                let lhs = try!(self.state.pop().ok_or("Could not find rhs for Add"));
                self.state.push(lhs + rhs);
            },
            OpCode::Sub => {
                let rhs = try!(self.state.pop().ok_or("Could not find lhs for Sub"));
                let lhs = try!(self.state.pop().ok_or("Could not find rhs for Sub"));
                self.state.push(lhs - rhs);
            },
            OpCode::Mul => {
                let rhs = try!(self.state.pop().ok_or("Could not find lhs for Mul"));
                let lhs = try!(self.state.pop().ok_or("Could not find rhs for Mul"));
                self.state.push(lhs * rhs);
            },
            OpCode::Div => {
                let rhs = try!(self.state.pop().ok_or("Could not find lhs for Div"));
                let lhs = try!(self.state.pop().ok_or("Could not find rhs for Div"));
                self.state.push(lhs / rhs);
            },
            OpCode::Mod => {
                let rhs = try!(self.state.pop().ok_or("Could not find lhs for Mod"));
                let lhs = try!(self.state.pop().ok_or("Could not find rhs for Mod"));
                self.state.push(lhs % rhs);
            },
            OpCode::Pow => {
                let rhs = try!(self.state.pop().ok_or("Could not find lhs for Pow"));
                let lhs = try!(self.state.pop().ok_or("Could not find rhs for Pow"));
                self.state.push(lhs.pow( rhs as u32));
            },
            OpCode::Compare(comp) => { 
                let rhs = try!(self.state.pop().ok_or("Could not find lhs for Compare"));
                let lhs = try!(self.state.pop().ok_or("Could not find rhs for Compare"));
                let res = match comp {
                    ast::BinOp::And => if (lhs != 0) && (rhs != 0) { 1 } else { 0 },
                    ast::BinOp::Or => if (lhs != 0) == (rhs != 0) { 1 } else { 0 },
                    ast::BinOp::Eq => if lhs == rhs { 1 } else { 0 },
                    ast::BinOp::Neq => if lhs != rhs { 1 } else { 0 },
                    ast::BinOp::Less => if lhs < rhs { 1 } else { 0 },
                    ast::BinOp::LessEqual => if lhs <= rhs { 1 } else { 0 },
                    ast::BinOp::Greater => if lhs > rhs { 1 } else { 0 },
                    ast::BinOp::GreaterEqual => if lhs >= rhs { 1 } else { 0 },
                    _ => panic!("Could not handle a comparison with this operator."),
                };
                self.state.push(res);
            }
            OpCode::Jump(dest) => return Ok(dest),
            OpCode::JumpIfTrue(dest) => {
                let comp_res = try!(self.state.pop().ok_or(
                        "Could not find comparison result for JumpIfTrue"));
                if comp_res != 0 {
                    return Ok(dest);
                }
            },
            OpCode::JumpIfFalse(dest) => {
                let comp_res = try!(self.state.pop().ok_or(
                        "Could not find comparison result for JumpIFalse"));
                if comp_res == 0 {
                    return Ok(dest);
                }
            },
            OpCode::CallFunction(num_args) => { 
                let begin = self.state.len() - num_args;
                let args : Vec<i64> = self.state.drain(begin..).collect();
                let func_name = self.state.pop().ok_or(
                    "Could not find function name on the stack for the function")?;
                // TODO: refactor flow of control to manage functions.
                panic!("Function calls are not yet supported.");
            },
            OpCode::Print => {
                let out = try!(self.state.pop().ok_or("Attempted to print an empty stack!"));
                println!("{}", out);
            }
            e => panic!(format!("Unsupported instruction: {:?}", e)),
        }
        Ok(1)
    }
}


impl SymbolTable {
    fn get(&mut self, name: &str) -> Val {
        let val = self.0.entry(name.to_string()).or_insert(Val::Int(0));
        val.clone()
    }

    fn set(&mut self, name: &str, val: Val) -> () {
        self.0.insert(name.to_string(), val);
    }
}

#[derive(Debug)]
pub struct Block {
    pub ops: Vec<OpCode>,
}

impl Block { 
    fn from_ast(block : &ast::Block) -> Result<Block> {
        let mut compiler = compile::Compiler::new();
        try!(compiler.compile_block(block));
        let compiled_block = Block{ ops: compiler.ops };
        Ok(compiled_block)
    }
}

#[derive(Debug)]
pub struct RegexBlock {
    regex: Regex,
    block: Block,
}

pub struct VM { 
    symbols: SymbolTable,
    begin: Vec<Block>,
    always: Vec<Block>,
    end: Vec<Block>,
    regex: Vec<RegexBlock>,

    stack: Stack,
    registers: [Val; 5],
    ret: i64,
}

impl VM { 
    pub fn new(program: ast::Program) -> Result<VM> {
        let mut begin = Vec::new();
        let mut end = Vec::new();
        let mut always = Vec::new();
        let mut regex = Vec::new();
        for block in &program.pattern_actions {
            match block.patact {
                ast::PatAction::Begin => begin.push(Block::from_ast(&block.block)?),
                ast::PatAction::End => end.push(Block::from_ast(&block.block)?),
                ast::PatAction::Always => always.push(Block::from_ast(&block.block)?),
                ast::PatAction::Regex(ref rx) => {
                    regex.push(RegexBlock{
                        regex:  Regex::new(rx).chain_err(||format!("Could not compile regex: \"{}\"", rx))?,
                        block: Block::from_ast(&block.block)?,
                    })
                },
            }
        }

        Ok(VM { 
            symbols: SymbolTable(HashMap::new()),
            begin: begin,
            always: always,
            end: end,
            regex: regex,
            stack: Stack::new(),
            registers: [Val::Int(0), Val::Int(0), Val::Int(0), Val::Int(0), Val::Int(0)],
            ret: 0
        })
    }

    pub fn eval_begin(&mut self) -> Result<()> {
        for block in &self.begin { 
            try!(VM::eval_block(&mut self.stack, block));
        }
        Ok(())
    }

    pub fn eval_end(&mut self) -> Result<()> {
        for block in &self.end { 
            try!(VM::eval_block(&mut self.stack, block));
        }
        Ok(())
    }


    fn eval_block(stack: &mut Stack, block: &Block) -> Result<()> {
        let mut program_counter = 0;
        while program_counter < block.ops.len() {
            let op = try!(block.ops.get(program_counter).ok_or("Could not find next instruction."));
            let jump = try!(stack.run_instr(op.clone()));
            // Mixing i64 and usize, there is a chance of jumping before the beginning of the 
            // program.
            program_counter = ((program_counter as i64) + jump) as usize;
        }
        Ok(())
    }

    pub fn eval_record(&mut self, record: &Record) -> Result<()> {
        for block in &self.always {
            try!(VM::eval_block(&mut self.stack, block));
        }

        if !self.regex.is_empty() {
            let line = str::from_utf8(&record.record).chain_err(||
                "Could not perform regex on non utf8 string")?;
            for block in &self.regex {
                if block.regex.is_match(line) {
                    try!(VM::eval_block(&mut self.stack, &block.block));
                }
            }
        }
        Ok(())
    }
}
