pub const KEYWORDS : &'static [&'static str] = &[
    "BEGIN", "END", "if", "else", "while", "do", "for", "in", "break", "continue", "delete",
    "next", "nextfile", "function", "func", "exit"
];

#[derive(Clone, Debug)]
pub struct Program {
    pub functions: Vec<Function>,
    pub pattern_actions: Vec<PatBlock>,
}

impl Program {
    pub fn from_top_level_blocks(blocks: Vec<TopLevelBlock>) -> Program {
        let mut functions = Vec::new();
        let mut pattern_actions = Vec::new();
        for b in blocks.into_iter() {
            match b {
                TopLevelBlock::Function(f) => functions.push(f),
                TopLevelBlock::PatBlock(p) => pattern_actions.push(p),
            }
        }
        Program { functions: functions, pattern_actions: pattern_actions }
    }
}

#[derive(Clone, Debug)]
pub enum TopLevelBlock {
    Function(Function),
    PatBlock(PatBlock),
}

#[derive(Clone, Debug)]
pub enum PatAction { Always, Begin, End, Regex(String) }

#[derive(Clone, Debug)]
pub struct PatBlock {
    pub patact: PatAction,
    pub block: Block
}

impl PatBlock {
    pub fn from_block(patact: PatAction, block: Block) -> PatBlock {
        PatBlock{patact: patact, block: block}
    }
}

#[derive(Clone, Debug)]
pub enum Statement {
    Expr(Expr),
    If(IfStatement),
    Print(Expr),
    WhileLoop(WhileLoop),
    DoLoop(DoLoop),
    ForLoop(Box<ForLoop>), // boxed because it's so big compared to other variants.
    Break,
    Continue,
    Next,
}

impl Statement {
    pub fn from_if(cond: Expr, then: Block, else_: Option<Block>) -> Statement {
        Statement::If(IfStatement{
            cond: cond,
            then: then,
            else_: else_
        })
    }
    pub fn from_for_loop(init: Option<Expr>, cond: Option<Expr>, inc: Option<Expr>, body: Block) -> Statement {
        Statement::ForLoop(Box::new(ForLoop{
            init: init,
            cond: cond,
            inc: inc,
            body: body,
        }))
    }

    pub fn from_while_loop(cond: Expr, body: Block) -> Statement {
        Statement::WhileLoop(WhileLoop{
            cond: cond,
            body: body,
        })
    }

    pub fn from_do_loop(cond: Expr, body: Block) -> Statement {
        Statement::DoLoop(DoLoop{
            cond: cond,
            body: body,
        })
    }
}

#[derive(Clone, Debug)]
pub struct ForLoop {
    pub init: Option<Expr>,
    pub cond: Option<Expr>,
    pub inc: Option<Expr>,
    pub body: Block,
}

#[derive(Clone, Debug)]
pub struct WhileLoop {
    pub cond: Expr,
    pub body: Block,
}

#[derive(Clone, Debug)]
pub struct DoLoop {
    pub cond: Expr,
    pub body: Block,
}


#[derive(Clone, Debug)]
pub struct IfStatement {
    pub cond: Expr,
    pub then: Block,
    pub else_: Option<Block>
}

#[derive(Clone, Debug)]
pub struct Block {
    pub statements: Vec<Statement>
}

#[derive(Clone, Copy, Debug)]
pub enum AssOp { Assign, Add, Sub, Mul, Div, Mod }

#[derive(Clone, Copy, Debug)]
pub enum UnaryExprOp { Negate, Not }

#[derive(Clone, Copy, Debug)]
pub enum UnaryIncrOp { PostIncr, PostDecr, PreIncr, PreDecr }

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum BinOp { Add, Sub, Mul, Div, Mod, Pow, And, Or,
    Eq, Neq, Less, LessEqual, Greater, GreaterEqual }

#[derive(Clone, Debug)]
pub struct Id(pub String);

#[derive(Clone, Debug)]
pub enum Expr {
    Number(i64),
    Id(Id),
    BinExpr(BinExpr),
    UnaryIncr(UnaryIncr),
    UnaryExpr(UnaryExpr),
    Assignment(Assignment),
    FunCall(FunCall),
}

#[derive(Clone, Debug)]
pub struct Assignment {
    pub op: AssOp,
    pub lvalue: Id,
    pub rvalue: Box<Expr>,
}


/// Unary Expression that involves a side effect on an identifier.
#[derive(Clone, Debug)]
pub struct UnaryIncr {
    pub op: UnaryIncrOp,
    pub lvalue: Id,
}

/// Unary Expression
#[derive(Clone, Debug)]
pub struct UnaryExpr {
    pub op: UnaryExprOp,
    pub expr: Box<Expr>,
}

#[derive(Clone, Debug)]
pub struct BinExpr {
    pub op: BinOp,
    pub lhs: Box<Expr>,
    pub rhs: Box<Expr>,
}

#[derive(Clone, Debug)]
pub struct FunCall {
    pub name: Id,
    pub args: Vec<Expr>
}

impl Expr {
    pub fn from_binexpr(op: BinOp, lhs: Expr, rhs: Expr) -> Expr {
        let e = Expr::BinExpr(BinExpr {
            op: op,
            lhs: Box::new(lhs),
            rhs: Box::new(rhs),
        });
        e.simplify()
    }

    pub fn from_unary_incr(op: UnaryIncrOp, lvalue: Id) -> Expr {
        Expr::UnaryIncr(UnaryIncr {
            op: op,
            lvalue: lvalue,
        })
    }

    pub fn from_unary(op: UnaryExprOp, expr: Expr) -> Expr {
        Expr::UnaryExpr(UnaryExpr {
            op: op,
            expr: Box::new(expr)
        })
    }

    pub fn from_assign(op: AssOp, lvalue: Id, rvalue: Expr) -> Expr {
        Expr::Assignment(Assignment{
            op: op,
            lvalue: lvalue,
            rvalue: Box::new(rvalue),
        })
    }

    pub fn from_funcall(name: Id, args: Vec<Expr>) -> Expr {
        Expr::FunCall(FunCall{
            name: name,
            args: args
        })
    }

    fn simplify(self) -> Expr {
        match self {
            Expr::BinExpr(BinExpr{op, lhs, rhs}) =>
                match (&*lhs, &*rhs) {
                    (&Expr::Number(x), &Expr::Number(y)) => {
                        let z = match op {
                            BinOp::Add => x + y,
                            BinOp::Sub => x - y,
                            BinOp::Mul => x * y,
                            BinOp::Div => x / y,
                            BinOp::Mod => x % y,
                            BinOp::Pow => x.pow(y as u32),
                            BinOp::And => if (x != 0)  && (y != 0) { 1 } else { 0 },
                            BinOp::Or => if (x != 0) || (y != 0) { 1 } else { 0 },
                            BinOp::Eq => if x ==  y { 1 } else { 0 },
                            BinOp::Neq => if x !=  y { 1 } else { 0 },
                            BinOp::Less => if x <  y { 1 } else { 0 },
                            BinOp::LessEqual => if x <=  y { 1 } else { 0 },
                            BinOp::Greater => if x > y { 1 } else { 0 },
                            BinOp::GreaterEqual => if x >=  y { 1 } else { 0 },
                        };
                        Expr::Number(z)
                    },
                    (_, _) => Expr::BinExpr(BinExpr{op: op, lhs: lhs, rhs: rhs}),
                },
            _ => self,
        }
    }

    #[cfg(test)]
    fn val(&self) -> Result<i64, String> {
        if let &Expr::Number(x) = self {
            Ok(x)
        } else {
            Err(format!("Expression could not be simplified: {:?}", self))
        }
    }
}

#[derive(Clone, Debug)]
pub struct Function {
    pub name: Id,
    pub args: Vec<Id>,
    pub body: Block,
}

impl Function {
    pub fn new(name: Id, args: Vec<Id>, body: Block) -> Function {
        Function {
            name: name,
            args: args,
            body: body
        }
    }
}

#[cfg(test)]
mod test {
    use grammar::*;

    macro_rules! test_parse {
        ( $parser:ident, $x:expr ) => {{
            let parsable = $x;
            let result = $parser(parsable);
            if let &Err(ParseError{line, column, ref expected, ..}) = &result {
                for (l, row) in parsable.lines().enumerate() {
                    if l+1 == line {
                        println!("Error: {}:", expected.iter().map(|x|*x).collect::<String>());
                        println!("{}", row);
                        let space = String::from_utf8(vec![b' '; column]).unwrap();
                        println!("{}^", space);
                    }
                }
            }
            assert!(result.is_ok());
        }}
    }

    #[test]
    fn test_arith() {
        assert_eq!(expression("1+1").unwrap().val(), Ok(2));
        assert_eq!(expression("5*5").unwrap().val(), Ok(25));
        assert_eq!(expression("1 + 1 + 1").unwrap().val(), Ok(3));
        assert_eq!(expression("2^2").unwrap().val(), Ok(4));
        assert_eq!(expression("2^2^2^2").unwrap().val(), Ok(65536));
        assert_eq!(expression("1024/2/2/2").unwrap().val(), Ok(128));
        assert_eq!(expression("2^2^2*2").unwrap().val(), Ok(32));
        assert_eq!(expression("2^2^(2*2)").unwrap().val(), Ok(65536));
        assert_eq!(expression("222+3333").unwrap().val(), Ok(3555));
        assert_eq!(expression("2+3*4").unwrap().val(), Ok(14));
        assert_eq!(expression("(2+2)*3").unwrap().val(), Ok(12));
        assert_eq!(expression("3*(2+2)").unwrap().val(), Ok(12));
        assert!(expression("(22+)+1").is_err());
        assert!(expression("1++1").is_err());
        assert!(expression("3)+1").is_err());
        test_parse!(expression, "$1 + $2");
        test_parse!(expression, "$NF");
    }

    #[test]
    fn test_unary_incr() {
        test_parse!(expression, "++x");
        test_parse!(expression, "--x");
        test_parse!(expression, "x++");
        test_parse!(expression, "x--");
        test_parse!(expression, "x-- > --y");
        test_parse!(expression, "x-- + --y");
    }

    #[test]
    fn test_unary_expr() {
        test_parse!(expression, "!x");
        test_parse!(expression, "!(1+1)");
        test_parse!(expression, "!(0) - !(1+1)");
        test_parse!(expression, "!(0) - !-(1+1)");
    }

    #[test]
    fn test_comp() {
        test_parse!(expression, "1==1");
        test_parse!(expression, "1 == 1");
        test_parse!(expression, "2 != i");
        test_parse!(expression, "i > 1");
        test_parse!(expression, "2 >= 1");
        test_parse!(expression, "2 < 1");
        test_parse!(expression, "2 <= 1");
        test_parse!(expression, "2 <= (1 + 2)");
        test_parse!(expression, "(1+2) <= (1 + 2)");
        test_parse!(expression, "x !=  y");
    }

    #[test]
    fn test_assign() {
        test_parse!(assignment, "i=2");
        test_parse!(assignment, "i = 2");
        test_parse!(assignment, "i += 2");
        test_parse!(assignment, "i -= 2");
        test_parse!(assignment, "i *= 2");
        test_parse!(assignment, "i /= 2");
        test_parse!(assignment, "i %= 2");
        test_parse!(assignment, "i = i");
        test_parse!(assignment, "i += i");
        test_parse!(assignment, "i = 2 * 1");
        test_parse!(assignment, "i = (2 * 1) / 2");
        assert!(assignment("while = (2 * 1) / 2").is_err());
    }

    #[test]
    fn test_statement() {
        test_parse!(statement, "i=2;");
        test_parse!(statement, "i = 2\n");
        test_parse!(statement, "i += 2\n");
        test_parse!(statement, "i -= 2;");
        test_parse!(statement, "i *= 2 ;");
        test_parse!(statement, "i *= 2 \n");
        test_parse!(statement, "doit = 1;"); // shouldn't parse the 'do' as keyword.
        assert!(statement("while = (2 * 1) / 2;").is_err());
    }

    #[test]
    fn test_if_statement() {
        test_parse!(if_statement, "if ( x > 3) { x += 1 }");
        test_parse!(if_statement, "if ( x > 3) { x += 1 } else { x *= 2 }");
        test_parse!(if_statement, "if ( x > 3) x += 1; else { x *= 2 }");
        test_parse!(if_statement, "if ( x > 3) x += 1; else x *= 2;");
        test_parse!(if_statement, "if ( x > 3){  x += 1; }else x *= 2;");
    }

    #[test]
    fn test_nested_if_statement() {
        test_parse!(if_statement, "if ( x > 3) if(y) x += 1\n else x *= 2;");
    }

    #[test]
    fn test_for_loop() {
        test_parse!(loop_statement, "for(;;);");
        test_parse!(loop_statement, "for(;;)\n\t;");
        test_parse!(loop_statement, "for(;i < 10;) print i;");
        test_parse!(loop_statement, "for (; i < 10;) {\n\tprint i\n}");
        test_parse!(loop_statement, "for (i=0; i < 10; i += 1) {\n\tprint i\n}");
        test_parse!(loop_statement, "for (i=0; i < 10; i++) {\n\tprint i\n}");
        test_parse!(loop_statement, "for (i=10; i > 0; i--) {\n\tprint i\n}");
    }

    #[test]
    fn test_while_loop() {
        test_parse!(loop_statement, "while(1);");
        test_parse!(loop_statement, "while(i < 10)\n\tprint i;");
        test_parse!(loop_statement, "while(i < 10)\n\tprint i;");
    }

    #[test]
    fn test_do_loop() {
        test_parse!(loop_statement, "do\n;\nwhile(1)");
        test_parse!(loop_statement, "do\n\tprint i\n while(i<10)");
        test_parse!(loop_statement, "do\n\tprint i;while(i<10)");
        test_parse!(loop_statement, "do {\n\tx++\n}while(i < 10)");
    }

    #[test]
    fn test_print() {
        test_parse!(statement, "print i\n");
        test_parse!(statement, "print 1\n");
        test_parse!(statement, "print $1\n");
        test_parse!(statement, "print 1+1\n");
    }

    #[test]
    fn test_pattern_action() {
        test_parse!(pattern_action, "/ /");
        test_parse!(pattern_action, "/hello/");
        test_parse!(pattern_action, "/.*/");
        test_parse!(pattern_action, "/^2017-03-../");
        test_parse!(pattern_action, r#"/\//"#);
        test_parse!(pattern_action, r#"/2017\/\/03\/16/"#);
        assert!(pattern_action("///").is_err());
        assert!(pattern_action("/.*//").is_err());
        assert!(pattern_action("/2017/03/16/").is_err());
    }

    #[test]
    fn test_patblock() {
        test_parse!(pattern_block, "{ sum = 4; x = 7}");
        test_parse!(pattern_block, "{\n sum = 4\n x = 7\n}");
        test_parse!(pattern_block, "{\n\tsum = 4\n\tx = 7\n}");
        test_parse!(pattern_block, "{ sum = 4   \n x = 7}");
        test_parse!(pattern_block, "{\tsum = 4   \n\n\tx = 7}");
        test_parse!(pattern_block, "BEGIN { sum+1; }");
        test_parse!(pattern_block, "END { sum+1; }");
        test_parse!(pattern_block, "/ / { sum+1; }");
    }

    #[test]
    fn test_program() {
        test_parse!(program, "{ sum += 4; } END { sum+1; }");
        test_parse!(program, r#"BEGIN { sum = 0; }
{
    sum += 4;
}
END {
    sum  +1;
}"#);
    }

    #[test]
    fn test_comments() {
        test_parse!(program, r#"BEGIN { sum = 0; }
{
    # sum = 0 # old code...
    sum += 1;
}
END {
    #Print the sum..
    print sum;
}"#);
    }
}
