#![recursion_limit = "1024"]

extern crate clap;
extern crate csv;
#[macro_use]
extern crate error_chain;
extern crate regex;
extern crate rustc_serialize;

use std::fs::File;
use std::io::Read;

use clap::{App, Arg, ArgGroup, ArgMatches};

mod ast;
mod compile;
mod errors;
mod input;
mod vm;

mod grammar {
    include!(concat!(env!("OUT_DIR"), "/grammar.rs"));
}

use errors::*;

pub use input::Reader;
pub use vm::VM;

fn parse_program(input: &str) -> Result<ast::Program> {
    let trimmed_input = input.trim();
    let prog = grammar::program(trimmed_input);
    if let Err(grammar::ParseError{line, column, ref expected, ..}) = prog {
        for (l, row) in input.lines().enumerate() {
            if line == l-1 || line == l+1 {
                println!("{}", row);
            } 
            if l+1 == line {
                println!("Error: {}:", expected.iter().map(|x|*x).collect::<String>());
                println!("{}", row);
                let space = String::from_utf8(vec![b' '; column]).unwrap();
                println!("{}^", space);
            }
        }
    }
    prog.chain_err(||"Could not parse program")
}


fn run(args: &ArgMatches) -> Result<()> {
    // This is completely wrong as it only uses one character and not a nice record breaking
    // system.
	let delim = args.value_of("field_separator").unwrap_or(" ").as_bytes()[0];
	let program = if let Some(script) = args.value_of("script") {
			try!(parse_program(&script)
				 .chain_err(|| "Could not create a program from the script string."))
		} else if let Some(script_file) = args.value_of("script-file") {
			let mut script = String::new();
			let mut file = try!(File::open(&script_file)
                                .chain_err(||"Could not open script file."));
			try!(file.read_to_string(&mut script)
                 .chain_err(|| "Could not read script file."));
			try!(parse_program(&script)
				 .chain_err(|| "Could not create a program from the script file."))
		} else {
		    panic!("Define a script on the command line or a script file (not both!!!)")
        };

    let input_file = args.value_of("INPUT").unwrap_or("/dev/stdin");
	let rdr = if input_file == "-" {
        try!(input::Reader::from_file("/dev/stdin")
                           .chain_err(|| "Could not create CSV Reader"))
            .delimiter(delim)
    } else {
        try!(input::Reader::from_file(&input_file)
                           .chain_err(|| "Could not create CSV Reader"))
            .delimiter(delim)
    };

	let mut vm = try!(vm::VM::new(program));
	vm.eval_begin()?;
    for rec in rdr {
        let rec = rec?;
        vm.eval_record(&rec)?;
    }
	vm.eval_end()?;
	Ok(())
}

fn main() {
	let matches = App::new("ripawk")
        .version("0.1")
        .author("Ewan Higgs <ewan.higgs@gmail.com>")
        .about("An orgy of one liners")
        .arg(Arg::with_name("field-separator")
             .short("F")
             .long("field-separator")
             .value_name("SEP")
             .required(false)
             .help("Field separator")
             .takes_value(true))
        .arg(Arg::with_name("script-file")
            .help("Script as a file")
             .short("f")
            .required(false)
            .takes_value(true))
        .arg(Arg::with_name("script")
             .help("Script as a one liner")
             .required(false)
             .index(1))
        .group(ArgGroup::with_name("script-group")
               .args(&["script-file", "script"])
               .multiple(false))
        .arg(Arg::with_name("INPUT")
             .help("Sets the input file to use")
             .required(false)
			 .index(2))
        .arg(Arg::with_name("csv")
             .help("Use CSV escaping")
             .required(false))
		.arg(Arg::with_name("v")
			 .short("v")
			 .multiple(true)
			 .help("Sets the level of verbosity"))
		.get_matches();

    if let Err(ref e) = run(&matches) {
        println!("error: {}", e);
        for e in e.iter().skip(1) {
            println!("caused by: {}", e);
        }
        if let Some(backtrace) = e.backtrace() {
            println!("backtrace: {:?}", backtrace);
        }
        ::std::process::exit(1);
    }
}

#[cfg(test)]
mod test {
    use super::input::Reader;
    use super::vm::VM;
    use super::parse_program;
    #[test]
    fn test_whole() {
        let input = "\
1 2 3 4
5 6 7 8
8 9 9 10
";
        let rdr = Reader::from_string(input);

        let program_src = r#"
BEGIN { sum = 0; } 
{ sum += 3 }
END { print sum }
"#;
        let prog = parse_program(program_src).unwrap();
        let mut vm = VM::new(prog).unwrap();
        vm.eval_begin().unwrap();
        for rec in rdr {
            vm.eval_record(&mut rec.unwrap()).unwrap();
        }
        vm.eval_end().unwrap();
    }
}
