# Ripawk 
"An orgy of one-liners" - Bryan Cantrill

## Introduction
Ripawk is an implementation of AWK that is intented to be performant and
embeddable. The ability to embed Ripawk means arbitrary record types could be
processed instead of delimited text.

# Status
Prototype quality

# What exists?
1. Blocks
2. Pattern matched blocks
3. Loops, conditions, integer variables

# What is missing?
1. String, float variables
2. `next`, `nextfile`
3. Postfix incrementation
4. Functions.
5. Accessors to the actual field records (`$1, $2`, etc.)
6. Loads more
