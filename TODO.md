TODO in terms of priority.

* Functions - for Rust functions how do we turn a vector of arguments into a
  funcall?
** Call, Return - functions need to be in the same address space and jumps must
be allowed to be absolute. Or functions are variables and we store the address
in the variable name. Makes sense as a PIE style approach afaict.
** Treat variable names in arguments as shadows of global names.
* Ints, Floats, Strings
* builtin variables ($0, $1, NF, etc)
** ~ operator
* break/continue
** How to we plumb through the place where we will jump to? Probably refactor
* Benches / performance.
** Turn strings from String objects copied everywhere to &'str or Cow.
** Better yet, when compiling, turn variables into integer offsets into stack and/or heap (i.e. memory
references) instead of lookups into a hash table.
** Do we need to return the number of vm lines added at each `compile_` function? Maybe we just check ops.len() before and after.
** Skip AST building and make the opcode stream while parsing?
the `compile_x` functions in src/compile to manager some kind of state with
begin/end of loops.
* Great error messages in parsing.
* There is currently no jit whatsoever. This could be a great performance boost
  since the event loop will no longer dispatch as it will be in hardware.
* Stdlib
* Try not to depend on nightly. Dynasm is a blocker for that atm, afaics.
* Const folding for associative operations. Currently it works for `1+1+1+x` but not `x+1+1+1+1`
* expr blocks
* Arrays

Useful one line programs. These must run.
http://www.math.utah.edu/docs/info/gawk_4.html#SEC24

See the output of C programs with different compilers. Maybe useful for JIT:
https://gcc-explorer.uplinklabs.net/
