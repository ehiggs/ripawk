BEGIN { 
    sum = 0  #Initilization isn't nessesary.
}
#run on each line
{ 
    sum += 1  # Increment
} 
END { print sum }
